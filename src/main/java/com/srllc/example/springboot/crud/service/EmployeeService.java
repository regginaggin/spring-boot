package com.srllc.example.springboot.crud.service;

import java.util.List;

import com.srllc.example.springboot.crud.model.Employee;

public interface EmployeeService {

  Employee saveEmployee(Employee employee);

  List<Employee> getEmployeeList();

  Employee updateEmployee(Employee employee, Long employeeId);

  void deleteEmployeeById(Long employeeId);

}
