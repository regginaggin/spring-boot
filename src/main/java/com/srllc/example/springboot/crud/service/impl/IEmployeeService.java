package com.srllc.example.springboot.crud.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import com.srllc.example.springboot.crud.model.Employee;
import com.srllc.example.springboot.crud.repository.EmployeeRepository;
import com.srllc.example.springboot.crud.service.EmployeeService;

import org.springframework.beans.factory.annotation.Autowired;

@Service
public class IEmployeeService implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
    public List<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public Employee updateEmployee(Employee employee, Long employeeId) {
        
        Optional<Employee> currentEmployee = employeeRepository.findById(employeeId);

        Employee emp = currentEmployee.get();
        emp.setName(employee.getName());

        return employeeRepository.save(emp);
    }

    @Override
    public void deleteEmployeeById(Long employeeId) {
        employeeRepository.deleteById(employeeId);
    }
    
}