package com.srllc.example.springboot.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@OpenAPIDefinition(info =
    @Info(title = "Spring Boot CRUD API", version = "3.0", description = "Documentation Spring Boot CRUD API v3.0")
)
@SpringBootApplication
public class SpringBootJpaCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaCrudApplication.class, args);
	}

}
